# Quick start Instarction

### dependencies
```
64bit OS, Linux/Unix/Mac is recommended;
64bit JDK 1.8+;
Gradle 3.x;  Gradle需要5.6及以上否则编译报错(5.6.1已验证可正常编译)
4g+ free disk for Broker server
```

### download and build

```
download from git
unzip defibus-master.zip
cd defibus-master
gradle clean dist tar -x test

You can get a tar.gz package in directory named 'build'
```

### Deployment

deploy DeFiBusNamesrv
```
tar -zxvf DeFiBus_1.0.0.tar.gz
cd bin
sh runnamesrv.sh
```

deploy DeFiBusBroker
```
tar -zxvf DeFiBus_1.0.0.tar.gz
cd bin
sh runbroker.sh
```





常见问题

1、broker启动报内存不足，修改runbroker.sh  将内存设置小

https://blog.csdn.net/wangmx1993328/article/details/81536168

2、broker参数配置

conf目录下

namesrvAddr=192.168.50.11:9876
brokerClusterName=test
brokerName=broker1
brokerId=0
brokerRole=ASYNC_MASTER
autoCreateTopicEnable=false
useReentrantLockWhenPutMessage=true
storePathRootDir=/dev/shm/store
storePathCommitLog=/dev/shm/store/commitlog


各个参数的含义可以参考rocketmq节点的，例如：
```
#所属集群名字
brokerClusterName=rocketmq-cluster
#broker名字，名字可重复,为了管理,每个master起一个名字,他的slave同他,eg:Amaster叫broker-a,他的slave也叫broker-a
brokerName=broker-a
#0 表示 Master，>0 表示 Slave
brokerId=0
#nameServer地址，分号分割
namesrvAddr=192.168.229.5:9876;192.168.229.6:9876
```



3、验证

这里是例子

https://gitee.com/zhaoyansheng/DeFiBus/tree/master/defibus-examples



4、管理平台安装

执行如下：
docker run -d -e "JAVA_OPTS=-Drocketmq.namesrv.addr=192.168.50.11:9876 -Dcom.rocketmq.sendMessageWithVIPChannel=false" -p 8090:8080 --name rmqconsole -t styletang/rocketmq-console-ng

登陆：http://192.168.50.11:8090/



10、参考内容：

https://github.com/WeBankFinTech/DeFiBus/wiki/DeFiBus&EventMesh-SIG



11、问题清单：

11.1 对应的rocketmq管理台的版本是多少，是否有开源？  defibus web管理台是否有开源？
     可以使用rocketmq的管理台  rocketmq-console。服务治理系统目前尚未开源。



